from bottle import post, request, get, route, run, HTTPResponse, default_app
import requests, json, sys
import configparser
import psycopg2
import base64


#TO-DO:
#-Order Handling (On Hold/Cancel/Etc.)
#-Signifyd Webhook Handling
#-Pythonification
#-Flexibility Modifications (largely limited by 3DCart documentation's lack of descriptiveness)

#get our config info
config = configparser.ConfigParser()
config.read('config.ini')

storeHeaders = {
    'SecureUrl':config['StoreInfo']['SecureUrl'],
    'Token':config['StoreInfo']['Token'],
    'SecretKey':config['StoreInfo']['SecretKey']
}

sigHeaders = {
    'ApiKey':base64.b64encode(str.encode(config['SigInfo']['ApiKey'])).decode('ASCII'),
}

dbInfo = {
	'DbUser':config['DbInfo']['DbUser'],
	'DbPass':config['DbInfo']['DbPass'],
	'DbName':config['DbInfo']['DbName']
}

try:
	conn = psycopg2.connect("dbname='%s' user='%s' host='localhost' password='%s'" % (dbInfo['DbName'], dbInfo['DbUser'], dbInfo['DbPass']))
	cur = conn.cursor()
except:
	print('invalid database credentials entered')

class httpHeaders:
	signifyd = {'Content-type':'application/json', 'Authorization':'Basic %s' % sigHeaders['ApiKey']}

	store = {'Content-type':'application/json', 'SecureUrl':storeHeaders['SecureUrl'], 'Token':storeHeaders['Token'], 'PrivateKey':storeHeaders['SecretKey']}



def sigOrder(odata):
    #optimally in the future this can be handled better than hardcoding but this will work for now
    jsonData = {
        "purchase":{
            "browserIpAddress":odata['IP'],
            "orderId":odata['OrderID'],
            "paymentMethod":odata['BillingPaymentMethod'],
            "transactionId":odata['TransactionList'][0]['TransactionID'],
            "avsResponseCode":odata['TransactionList'][0]['TransactionAVS'],
            "cvvResponseCode":odata['TransactionList'][0]['TransactionCVV2'][0],
            "totalPrice":odata['OrderAmount'],
            "products":[]
        },
        "recipient":{
            "fullName":"%s %s" % (odata['ShipmentList'][0]['ShipmentFirstName'], odata['ShipmentList'][0]['ShipmentLastName']),
            "confirmationEmail":odata['ShipmentList'][0]['ShipmentEmail'],
            "confirmationPhone":odata['ShipmentList'][0]['ShipmentPhone'],
            "organization":odata['ShipmentList'][0]['ShipmentCompany'],
            "deliveryAddress":{
                "streetAddress":odata['ShipmentList'][0]['ShipmentAddress'],
                "unit":odata['ShipmentList'][0]['ShipmentAddress2'],
                "city":odata['ShipmentList'][0]['ShipmentCity'],
                "provinceCode":odata['ShipmentList'][0]['ShipmentState'],
                "postalCode":odata['ShipmentList'][0]['ShipmentZipCode'],
                "countryCode":odata['ShipmentList'][0]['ShipmentCountry']
            }},
        "card":{
            "cardHolderName":odata['CardName'],
            "last4":odata['CardNumber'][-4:],
            "expiryMonth":odata['CardExpirationMonth'],
            "expiryYear":odata['CardExpirationYear'],
            "billingAddress":{
                "streetAddress":odata['BillingAddress'],
                "unit":odata['BillingAddress2'],
                "city":odata['BillingCity'],
                "provinceCode":odata['BillingState'],
                "postalCode":odata['BillingZipCode'],
                "countryCode":odata['BillingCountry']
            }}}
    try:
        for i in range(len(odata['OrderItemList'])):
            item = odata['OrderItemList'][i]
            jsonData['purchase']['products'].append({
                "itemId":item['CatalogID'],
                "itemName":item['ItemID'],
                "itemQuantity":item['ItemQuantity'],
                "itemPrice":float(item['ItemUnitPrice'])+float(item['ItemOptionPrice'])
            })
    except:
        pass

    return json.dumps(jsonData)#return json formatted for push to signifyd

#order retrieval
def getOrder(onum):
    return requests.get('https://apirest.3dcart.com/3dcartWebAPI/v1/Orders/%s' % (onum), headers=httpHeaders.store).json()[0]


#new order webhook
@post('/order/new')
def newOrder():
    try:
        #print(request.json, file=sys.stderr)
        s = sigOrder(request.json[0])
        #print(request.json, file=sys.stderr)
        r = requests.post('https://api.signifyd.com/v2/cases', headers=httpHeaders.signifyd, data=s)
        if r.status_code == 201:
            return HTTPResponse(status=200)
        else:
            print(r.json, file=sys.stderr)
            return HTTPResponse(status=500)
    except Exception as e:
        print(str(e), file=sys.stderr)

@post('/case/new')
def newCase():
	case = request.json()[0]['caseId']
	pass

@post('/case/update')
def handleCase():
	pass

@get('/')
def status():
	return(json.dumps({'Status':'OK'}))

if __name__ == '__main__':
   run(host='127.0.0.1', port='8080')
else:
   app = application = default_app()
